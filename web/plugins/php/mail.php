<?php

    // Only process POST requests.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // Get the form fields and remove whitespace.
        $first_name = strip_tags(trim($_POST["customerName"]));
        $last_name = strip_tags(trim($_POST["customerLastName"]));
        $first_name = str_replace(array("\r","\n"),array(" "," "),$first_name);
		$last_name = str_replace(array("\r","\n"),array(" "," "),$last_name);
        $email = filter_var(trim($_POST["customerEmail"]), FILTER_SANITIZE_EMAIL);
        $phone = trim($_POST["customerPhone"]);
        $message = trim($_POST["contactMessage"]);

        // Check that data was sent to the mailer.
        if ( empty($first_name) OR empty($phone) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Por favor, completa el formulario e intenta nuevamente.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = "contacto@artelanasimperial.com";

        // Set the email subject.
        $subject = "Tienes un nuevo mensaje desde tu sitio web";

        // Build the email content.
        $email_content = "Nombre: $first_name\n";
        $email_content .= "Apellido: $last_name\n";
        $email_content .= "Correo: $email\n";
        $email_content .= "Telefono: $phone\n\n";
        $email_content .= "Mensaje:\n$message\n";
        $email_content .= "Saludos!";

        // Build the email headers.
        $email_headers = "De: $first_name $last_name <$email>\n";
        $email_headers .= "Cc: $email\n";
        // Send the email.
        if (mail($recipient, $subject, $email_content, $email_headers, $phone)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Gracias, tu mensaje ha sido enviado.";
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "¡Oops! Algo salió mal y no pudimos enviar tu mensaje.";
        }

    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Hubo un error al enviar el mensaje, por favor, intenta nuevamente.";
    }

?>
