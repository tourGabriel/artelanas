<?php

    // Only process POST requests.
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        // // Get the form fields and remove whitespace.\
        $email = filter_var(trim($_POST["customerEmail"]), FILTER_SANITIZE_EMAIL);

        // Check that data was sent to the mailer.
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            // Set a 400 (bad request) response code and exit.
            http_response_code(400);
            echo "Por favor, completa el formulario e intenta nuevamente.";
            exit;
        }

        // Set the recipient email address.
        // FIXME: Update this to your desired email address.
        $recipient = 'contacto@artelanasimperial.com'; // atención a la coma
       

        // Set the email subject.
        $subject = "Tienes un nuevo suscriptor!";
        $subjectSub = 'artelanasimperial.com';

        // Build the email content.
        $email_content = "El correo <$email> se ha suscrito para recibir noticias desde www.artelanasimperial.com\n";
        $email_content .= "Saludos!";

        $email_contentSub = "Te has suscrito satisfactoriamente a www.artelanasimperial.com\n";
        $email_contentSub .= "Saludos!";

        // Send the email.
        if (mail($recipient, $subject, $email_content)) {
            // Set a 200 (okay) response code.
            http_response_code(200);
            echo "Suscripción exitosa";
            mail($email, $subjectSub, $email_contentSub);
        } else {
            // Set a 500 (internal server error) response code.
            http_response_code(500);
            echo "¡Oops! Algo salió mal y no pudimos suscribirte.";
        }

        

        
    } else {
        // Not a POST request, set a 403 (forbidden) response code.
        http_response_code(403);
        echo "Hubo un error al intentar suscribirte, por favor, intenta nuevamente.";
    }

?>
